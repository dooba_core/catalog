/* Dooba SDK
 * Catalog Key-Value Storage
 */

#ifndef	__CATALOG_H
#define	__CATALOG_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Key/Value Delimiter
#define	CATALOG_DELIM							0x1e

// Buffer Size
#ifndef	CATALOG_BUFSIZE
#define	CATALOG_BUFSIZE							128
#endif

// Temp Catalog Path
#define	CATALOG_TMP								".tmp"

// Shortcuts
#define	catalog_touch(p)						catalog_touch_n(p, strlen(p))
#define	catalog_list(p, h, u)					catalog_list_n(p, strlen(p), h, u)
#define	catalog_read(p, k, v, vl)				catalog_read_n_k(p, strlen(p), k, strlen(k), v, vl)
#define	catalog_read_n(p, l, k, v, vl)			catalog_read_n_k(p, l, k, strlen(k), v, vl)
#define	catalog_read_k(p, k, kl, v, vl)			catalog_read_n_k(p, strlen(p), k, kl, v, vl)
#define	catalog_write(p, k, v)					catalog_write_n_k_v(p, strlen(p), k, strlen(k), v, strlen(v))
#define	catalog_write_n(p, l, k, v)				catalog_write_n_k_v(p, l, k, strlen(k), v, strlen(v))
#define	catalog_write_k(p, k, kl, v)			catalog_write_n_k_v(p, strlen(p), k, kl, v, vl)
#define	catalog_write_v(p, k, v, vl)			catalog_write_n_k_v(p, strlen(p), k, kl, v, vl)
#define	catalog_write_n_k(p, l, k, kl, v)		catalog_write_n_k_v(p, l, k, kl, v, strlen(v))
#define	catalog_write_n_v(p, l, k, v, vl)		catalog_write_n_k_v(p, l, k, strlen(k), v, vl)
#define	catalog_write_k_v(p, k, kl, v, vl)		catalog_write_n_k_v(p, strlen(p), k, kl, v, vl)
#define	catalog_remove(p, k)					catalog_remove_n_k(p, strlen(p), k, strlen(k))
#define	catalog_remove_n(p, l, k)				catalog_remove_n_k(p, l, k, strlen(k))
#define	catalog_remove_k(p, k, kl)				catalog_remove_n_k(p, strlen(p), k, kl)
#define	catalog_clear(p)						catalog_clear_n(p, strlen(p))

// Touch Catalog - Fixed Length
extern uint8_t catalog_touch_n(char *path, uint8_t path_len);

// List Entries - Fixed Length
extern uint8_t catalog_list_n(char *path, uint8_t path_len, void (*entry_handler)(void *user, char *key, uint16_t key_len, char *val, uint16_t val_len), void *user);

// Read Entry - Fixed Length
extern uint8_t catalog_read_n_k(char *path, uint8_t path_len, char *key, uint16_t key_len, char *val, uint16_t *val_len);

// Write Entry - Fixed Length
extern uint8_t catalog_write_n_k_v(char *path, uint8_t path_len, char *key, uint16_t key_len, char *val, uint16_t val_len);

// Remove Entry - Fixed Length
extern uint8_t catalog_remove_n_k(char *path, uint8_t path_len, char *key, uint16_t key_len);

// Clear Catalog - Fixed Length
extern uint8_t catalog_clear_n(char *path, uint8_t path_len);

#endif
