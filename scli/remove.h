/* Dooba SDK
 * Catalog Key-Value Storage
 */

#ifndef	__CATALOG_SCLI_REMOVE_H
#define	__CATALOG_SCLI_REMOVE_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Remove Key
extern void catalog_scli_remove(char **args, uint16_t *args_len);

#endif
