/* Dooba SDK
 * Catalog Key-Value Storage
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>
#include <vfs/vfs.h>

// Internal Includes
#include "catalog.h"
#include "scli/write.h"

// Write Key
void catalog_scli_write(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	char *path;
	uint16_t path_l;
	char *key;
	uint16_t key_l;

	// Acquire Path
	if(str_next_arg(args, args_len, &x, &l) == 0)							{ scli_printf("Please provide a valid path\n"); return; }
	if((l == 0) || (l > VFS_PATH_MAXLEN))									{ scli_printf("Please provide a valid path\n"); return; }
	path = x;
	path_l = l;

	// Acquire Key
	if(str_next_arg(args, args_len, &x, &l) == 0)							{ scli_printf("Please provide a valid key\n"); return; }
	if((l == 0) || (l > CATALOG_BUFSIZE))									{ scli_printf("Please provide a valid key\n"); return; }
	key = x;
	key_l = l;

	// Acquire Value
	if(str_next_arg(args, args_len, &x, &l) == 0)							{ scli_printf("Please provide a valid value\n"); return; }
	if((l == 0) || (l > CATALOG_BUFSIZE))									{ scli_printf("Please provide a valid value\n"); return; }

	// Read
	scli_printf("Writing [%t]...\n", path, path_l);
	if(catalog_write_n_k_v(path, path_l, key, key_l, x, l))					{ scli_printf("Write failed!\n"); return; }
}
