/* Dooba SDK
 * Catalog Key-Value Storage
 */

#ifndef	__CATALOG_SCLI_LIST_H
#define	__CATALOG_SCLI_LIST_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// List Contents
extern void catalog_scli_list(char **args, uint16_t *args_len);

// Entry Handler
extern void catalog_scli_list_handler(void *user, char *key, uint16_t key_len, char *val, uint16_t val_len);

#endif
