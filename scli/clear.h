/* Dooba SDK
 * Catalog Key-Value Storage
 */

#ifndef	__CATALOG_SCLI_CLEAR_H
#define	__CATALOG_SCLI_CLEAR_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Clear Contents
extern void catalog_scli_clear(char **args, uint16_t *args_len);

#endif
