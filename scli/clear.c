/* Dooba SDK
 * Catalog Key-Value Storage
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>
#include <vfs/vfs.h>

// Internal Includes
#include "catalog.h"
#include "scli/clear.h"

// Clear Contents
void catalog_scli_clear(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;

	// Acquire Path
	if(str_next_arg(args, args_len, &x, &l) == 0)						{ scli_printf("Missing path\n"); return; }
	if((l == 0) || (l > VFS_PATH_MAXLEN))								{ scli_printf("Missing path\n"); return; }

	// Clear Contents
	scli_printf("Clearing [%t]...\n", x, l);
	if(catalog_clear_n(x, l))											{ scli_printf("Clear failed!\n"); return; }
}
