/* Dooba SDK
 * Catalog Key-Value Storage
 */

#ifndef	__CATALOG_SCLI_READ_H
#define	__CATALOG_SCLI_READ_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Read Key
extern void catalog_scli_read(char **args, uint16_t *args_len);

#endif
