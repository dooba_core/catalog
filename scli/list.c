/* Dooba SDK
 * Catalog Key-Value Storage
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>
#include <vfs/vfs.h>

// Internal Includes
#include "catalog.h"
#include "scli/list.h"

// List Contents
void catalog_scli_list(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;

	// Acquire Path
	if(str_next_arg(args, args_len, &x, &l) == 0)						{ scli_printf("Missing path\n"); return; }
	if((l == 0) || (l > VFS_PATH_MAXLEN))								{ scli_printf("Missing path\n"); return; }

	// List Contents
	scli_printf("Reading [%t]...\n", x, l);
	if(catalog_list_n(x, l, catalog_scli_list_handler, 0))				{ scli_printf("Read failed!\n"); return; }
}

// Entry Handler
void catalog_scli_list_handler(void *user, char *key, uint16_t key_len, char *val, uint16_t val_len)
{
	// Print Entry
	scli_printf(" * [%t] -> [%t]\n", key, key_len, val, val_len);
}
