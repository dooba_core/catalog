/* Dooba SDK
 * Catalog Key-Value Storage
 */

#ifndef	__CATALOG_SCLI_WRITE_H
#define	__CATALOG_SCLI_WRITE_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Write Key
extern void catalog_scli_write(char **args, uint16_t *args_len);

#endif
