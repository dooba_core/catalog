/* Dooba SDK
 * Catalog Key-Value Storage
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>
#include <vfs/vfs.h>

// Internal Includes
#include "catalog.h"
#include "scli/remove.h"

// Remove Key
void catalog_scli_remove(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	char *path;
	uint16_t path_l;

	// Acquire Path
	if(str_next_arg(args, args_len, &x, &l) == 0)							{ scli_printf("Missing path\n"); return; }
	if((l == 0) || (l > VFS_PATH_MAXLEN))									{ scli_printf("Missing path\n"); return; }
	path = x;
	path_l = l;

	// Acquire Key
	if(str_next_arg(args, args_len, &x, &l) == 0)							{ scli_printf("Missing key\n"); return; }
	if((l == 0) || (l > CATALOG_BUFSIZE))									{ scli_printf("Missing key\n"); return; }

	// Remove
	scli_printf("Removing [%t] (%t)...\n", x, l, path, path_l);
	if(catalog_remove_n_k(path, path_l, x, l))								{ scli_printf("Remove failed!\n"); return; }
}
