/* Dooba SDK
 * Catalog Key-Value Storage
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>
#include <vfs/vfs.h>

// Internal Includes
#include "catalog.h"
#include "scli/read.h"

// Value Buffer
char catalog_scli_read_buf[CATALOG_BUFSIZE];

// Read Key
void catalog_scli_read(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	char *path;
	uint16_t path_l;
	uint16_t vl;

	// Acquire Path
	if(str_next_arg(args, args_len, &x, &l) == 0)							{ scli_printf("Missing path\n"); return; }
	if((l == 0) || (l > VFS_PATH_MAXLEN))									{ scli_printf("Missing path\n"); return; }
	path = x;
	path_l = l;

	// Acquire Key
	if(str_next_arg(args, args_len, &x, &l) == 0)							{ scli_printf("Missing key\n"); return; }
	if((l == 0) || (l > CATALOG_BUFSIZE))									{ scli_printf("Missing key\n"); return; }

	// Read
	scli_printf("Reading [%t]...\n", path, path_l);
	if(catalog_read_n_k(path, path_l, x, l, catalog_scli_read_buf, &vl))	{ scli_printf("Read failed!\n"); return; }
	if(vl == 0)																{ scli_printf("%t not found!\n", x, l); return; }
	scli_printf(" * [%t] -> [%t]\n", x, l, catalog_scli_read_buf, vl);
}
