/* Dooba SDK
 * Catalog Key-Value Storage
 */

// External Includes
#include <string.h>
#include <util/cprintf.h>
#include <vfs/vfs.h>

// Internal Includes
#include "catalog.h"

// File Descriptors
struct vfs_handle catalog_fd;
struct vfs_handle catalog_fd_tmp;

// Path Buffer
char catalog_path_buf[VFS_PATH_MAXLEN];

// Line Buffer
uint8_t catalog_buf[CATALOG_BUFSIZE];

// Touch Catalog - Fixed Length
uint8_t catalog_touch_n(char *path, uint8_t path_len)
{
	// Touch Catalog
	if(vfs_open_n(&catalog_fd, path, path_len, VFS_OPEN_CREATE))							{ return 1; }
	vfs_close(&catalog_fd);

	return 0;
}

// List Entries - Fixed Length
uint8_t catalog_list_n(char *path, uint8_t path_len, void (*entry_handler)(void *user, char *key, uint16_t key_len, char *val, uint16_t val_len), void *user)
{
	uint16_t len;
	uint16_t kl;

	// Touch
	if(catalog_touch_n(path, path_len))														{ return 1; }

	// Open Catalog File
	if(vfs_open_n(&catalog_fd, path, path_len, 0))											{ return 1; }

	// Run through Entries
	while(1)
	{
		// Read Entry
		if(vfs_gets(&catalog_fd, catalog_buf, CATALOG_BUFSIZE, &len))						{ vfs_close(&catalog_fd); return 0; }

		// Find Delimiter
		kl = 0;
		while((kl < len) && (catalog_buf[kl] != CATALOG_DELIM))								{ kl = kl + 1; }
		if(kl < len)																		{ entry_handler(user, (char *)catalog_buf, kl, (char *)&(catalog_buf[kl + 1]), len - (kl + 1)); }
	}

	return 0;
}

// Read Entry
uint8_t catalog_read_n_k(char *path, uint8_t path_len, char *key, uint16_t key_len, char *val, uint16_t *val_len)
{
	uint16_t len;
	uint16_t kl;

	// Clear Value Len
	*val_len = 0;

	// Touch
	if(catalog_touch_n(path, path_len))														{ return 1; }

	// Open Catalog File
	if(vfs_open_n(&catalog_fd, path, path_len, 0))											{ return 1; }

	// Find Entry
	while(*val_len == 0)
	{
		// Read Entry
		if(vfs_gets(&catalog_fd, catalog_buf, CATALOG_BUFSIZE, &len))						{ vfs_close(&catalog_fd); return 1; }

		// Find Delimiter
		kl = 0;
		while((kl < len) && (catalog_buf[kl] != CATALOG_DELIM))								{ kl = kl + 1; }
		if((kl < len) && (kl == key_len))
		{
			// Check Key
			if(memcmp(key, catalog_buf, kl) == 0)
			{
				// Extract Value
				*val_len = len - (kl + 1);
				if(*val_len)																{ memcpy(val, &(catalog_buf[kl + 1]), *val_len); }
			}
		}
	}

	// Close File
	vfs_close(&catalog_fd);

	return 0;
}

// Write Entry
uint8_t catalog_write_n_k_v(char *path, uint8_t path_len, char *key, uint16_t key_len, char *val, uint16_t val_len)
{
	uint16_t len;
	uint16_t kl;
	uint16_t tl;

	// Check Length
	if((key_len + val_len + 1) >= CATALOG_BUFSIZE)											{ return 1; }

	// Open Catalog Files
	tl = csnprintf(catalog_path_buf, VFS_PATH_MAXLEN, "%t%s", path, path_len, CATALOG_TMP);
	if(vfs_open_n(&catalog_fd, path, path_len, VFS_OPEN_CREATE))							{ return 1; }
	if(vfs_open_n(&catalog_fd_tmp, catalog_path_buf, tl, VFS_OPEN_CREATE))					{ vfs_close(&catalog_fd); return 1; }

	// Copy Catalog to Temp
	len = 1;
	while(len)
	{
		// Read Entry
		if(vfs_gets(&catalog_fd, catalog_buf, CATALOG_BUFSIZE, &len))						{ len = 0; }
		else
		{
			// Find Delimiter
			kl = 0;
			while((kl < len) && (catalog_buf[kl] != CATALOG_DELIM))							{ kl = kl + 1; }
			if((kl < len) && (kl == key_len))
			{
				// Check Key { Keep Entry only if different key / eliminate current entry }
				if(memcmp(key, catalog_buf, kl))											{ if(vfs_puts(&catalog_fd_tmp, "%t", catalog_buf, len)) { goto fail_both; } }
			}
			else																			{ if(vfs_puts(&catalog_fd_tmp, "%t", catalog_buf, len)) { goto fail_both; } }
		}
	}

	// Close File
	vfs_close(&catalog_fd);

	// Write Entry to Temp
	if(vfs_puts(&catalog_fd_tmp, "%t%c%t", key, key_len, CATALOG_DELIM, val, val_len))		{ goto fail_temp; }

	// Close Temp File
	vfs_close(&catalog_fd_tmp);

	// Move Temp File
	if(vfs_rmobj_n(path, path_len))															{ return 1; }
	if(vfs_mvobj_n(catalog_path_buf, tl, path, path_len))									{ return 1; }

	return 0;

	// On Error (with both files open)
	fail_both:

	// Close File
	vfs_close(&catalog_fd);

	// On Error (with only temp open)
	fail_temp:

	// Close & Drop Temp
	vfs_close(&catalog_fd_tmp);
	vfs_rmobj_n(catalog_path_buf, tl);

	// Fail
	return 1;
}

// Remove Entry
uint8_t catalog_remove_n_k(char *path, uint8_t path_len, char *key, uint16_t key_len)
{
	uint16_t len;
	uint16_t kl;
	uint16_t tl;

	// Open Catalog Files
	tl = csnprintf(catalog_path_buf, VFS_PATH_MAXLEN, "%t%s", path, path_len, CATALOG_TMP);
	if(vfs_open_n(&catalog_fd, path, path_len, 0))											{ return 1; }
	if(vfs_open_n(&catalog_fd_tmp, catalog_path_buf, tl, VFS_OPEN_CREATE))					{ vfs_close(&catalog_fd); return 1; }

	// Copy Catalog to Temp
	len = 1;
	while(len)
	{
		// Read Entry
		if(vfs_gets(&catalog_fd, catalog_buf, CATALOG_BUFSIZE, &len))						{ len = 0; }
		else
		{
			// Find Delimiter
			kl = 0;
			while((kl < len) && (catalog_buf[kl] != CATALOG_DELIM))							{ kl = kl + 1; }
			if((kl < len) && (kl == key_len))
			{
				// Check Key { Keep Entry only if different key / eliminate current entry }
				if(memcmp(key, catalog_buf, kl))											{ if(vfs_puts(&catalog_fd_tmp, "%t", catalog_buf, len)) { goto fail; } }
			}
			else																			{ if(vfs_puts(&catalog_fd_tmp, "%t", catalog_buf, len)) { goto fail; } }
		}
	}

	// Close Files
	vfs_close(&catalog_fd);
	vfs_close(&catalog_fd_tmp);

	// Move Temp File
	if(vfs_rmobj_n(path, path_len))															{ return 1; }
	if(vfs_mvobj_n(catalog_path_buf, tl, path, path_len))									{ return 1; }

	return 0;

	// On Error
	fail:

	// Close Files & Drop Temp
	vfs_close(&catalog_fd);
	vfs_close(&catalog_fd_tmp);
	vfs_rmobj_n(catalog_path_buf, tl);

	// Fail
	return 1;
}

// Clear Catalog
uint8_t catalog_clear_n(char *path, uint8_t path_len)
{
	// Reset Catalog
	if(vfs_open_n(&catalog_fd, path, path_len, VFS_OPEN_CREATE | VFS_OPEN_TRUNCATE))		{ return 1; }
	vfs_close(&catalog_fd);

	return 0;
}
